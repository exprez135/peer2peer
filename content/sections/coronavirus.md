---
title : "Coronavirus"
description: ""
draft: false
weight: 2
---

Due to the outbreak of COVID-19, Peer2Peer is going virtual for the rest of the 2019-2020 school year. Need help with the coronavirus? See BTW newspaper's page at https://thetaliaferrotimes.org/coronavirus/.
