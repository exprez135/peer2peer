---
title : "How"
description: ""
draft: false
weight: 3
---

Peer 2 Peer connects students at BTW to help each other. Together, we work towards improving the academic strength of every one of our peers at BTW. The process looks different for everyone: some go to meet friends from their classes to work together, some know exactly who they want to work with before they arrive, and some connect to a potential peer through [findhelp.ga](https://findhelp.ga). This website aims to ease the Peer 2 Peer process by connecting you with a student who has done well in the subject you are studying. We hope that you find this easy to use!
