---
title : "About"
description: ""
draft: false
weight: 1
---

Do you need help with homework, class lessons, AP or IB tests, ACT or SAT tests, class scheduling, or any academic advice? For questions big or small, ask a fellow hornet! After all, hornets don’t buzz alone!

Peer2Peer is a student-run peer tutoring organization built on the core improvement of academic strength throughout Booker T. A project of BTW's own National Honor Society, its fundamental purpose is to aid students who cannot otherwise find the help they need -- whether that be with homework, testing, or general academic tips.
