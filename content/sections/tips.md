---
title : "Tips"
description: ""
draft: false
weight: 4
---

Never be afraid to ask if you don’t understand something! Even if it’s the tiniest question, teachers and tutors will be glad to answer it.

Come with specific assignments, tests, and topics which you need help in. The more specific you are, the easier it will be to find help.

If you don't understand, ask again. If you don't understand, ask again. If you still don't understand, ask again. Your peers are at P2P to help you! Don't worry about annoying us.
