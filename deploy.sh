#!/bin/bash

echo -e "\033[0;32mDeploying updates to GitLab...\033[0m"

# Remove current public folder to ensure all outdated files are gone.
echo -e "\033[0;32mRemoving outdated public folder\033[0m"
rm -r public

# Build the project.
echo -e "\033[0;32mStarting Hugo build process...\033[0m"
hugo -d public #this folder for committing to exprez135.github.io
echo -e "\033[0;32mHugo build process complete.\033[0m"
# OUTDATED hugo -d publicOrg #this folder for committing to hugo.thetaliaferrotimes.org

# Add changes to directories outside of public to git
echo -e "\033[0;32mTracking all git files...\033[0m"
git add .

# Go To Public folder
echo -e "\033[0;32mMoving into pubic_html folder...\033[0m"
cd public || exit
# OUTDATED OR, go to publicOrg folder for hugo.thetaliaferrotimes.org
# OUTDATED cd publicOrg

# OUTDATED Add changes to git.
# OUTDATED echo -e "\033[0;32mEnsuring public_html files are tracked...\033[0m"
# OUTDATED git add .

# Commit changes.
echo -e "\033[0;32mCommitting changes...\033[0m"
msg="rebuilding site $(date):"
echo -e "\033[0;32mWhat is your custom commit message? Leave blank for none.\033[0m"
read -r usrmsg

if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$msg $usrmsg"
echo -e "\033[0;32mChanges committed.\033[0m"

# Push source and build repos.
echo -e "\033[0;32mPushing origin master...\033[0m"
git push origin master
echo -e "\033[0;32mPushed.\033[0m"

# Come Back up to the Project Root
echo -e "\033[0;32mMoving back into Project Root...\033[0m"
cd ..
echo -e "\033[0;32mPROCESS COMPLETED.\033[0m"
